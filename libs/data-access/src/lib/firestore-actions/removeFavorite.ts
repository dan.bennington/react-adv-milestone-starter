import { doc, deleteDoc } from 'firebase/firestore'
import { db, favoritesPath } from '../firestore'

export const removeFavorite = async (firebaseId: string) => {
  try {
    if(typeof firebaseId !== 'string') {
      throw new Error('invalid id')
    }
    await deleteDoc(doc(db, favoritesPath, firebaseId))
    return { message: 'success' }
  } catch(error) {
    return error
  }
}
