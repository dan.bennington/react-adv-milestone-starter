import { addFavorite } from './addFavorite'
import { transformBusinesses } from '@react-v2/utilities'
import { yelpBusinessSearchData } from '@react-v2/mock-data'

const businesses = transformBusinesses(yelpBusinessSearchData.businesses)

afterEach(() => {
  jest.resetAllMocks()
})

describe('addFavorite', () => {
  it('should return a success message and firebase name on successful requests', async () => {
    jest.mock('firebase/firestore', () => ({
      addDoc: () => new Promise(res => res(true))
    }))
    const resp = await addFavorite(businesses[0])
    expect(resp.message).toBe('success')
  })

  it('should return an error message if business is invalid', async () => {
    const business = { name: 15 }
    // @ts-expect-error testing validation
    const error = await addFavorite(business)
    expect(error).toBeInstanceOf(Error)
  })
})
