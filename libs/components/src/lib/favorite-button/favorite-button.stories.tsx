import { Story, Meta } from '@storybook/react';
import { FavoriteButton, FavoriteButtonProps } from './favorite-button';

export default {
  component: FavoriteButton,
  title: 'FavoriteButton',
} as Meta;

const Template: Story<FavoriteButtonProps> = (args) => (
  <FavoriteButton {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  addFavorite: () => console.log('clicked'),
  removeFavorite: () => console.log('clicked'),
  isFavorite: false,
};

export const Favorited = Template.bind({});
Favorited.args = {
  addFavorite: () => console.log('clicked'),
  removeFavorite: () => console.log('clicked'),
  isFavorite: true,
};
