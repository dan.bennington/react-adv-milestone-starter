import {
  ChangeEvent,
  FormEvent,
  useState
} from 'react';
import {
  Box,
  TextField
} from '@mui/material'
import { Search } from '@mui/icons-material'

export interface SearchInputProps {
  onSearch: (value: string | null) => void;
}

export function SearchInput({ onSearch }: SearchInputProps) {
  const [ value, setValue ] = useState('')
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target
    setValue(value)
    if(value === '') onSearch(null)
  }
  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    onSearch(value)
  }
  return (
    <Box
      component='form'
      noValidate
      onSubmit={handleSubmit}
    >
      <TextField
        type='search'
        onChange={handleChange}
        value={value}
        InputProps={{
          startAdornment: <Search />
        }}
        sx={{
          width: '100%',
          '& .MuiInputBase-input': {
            paddingLeft: '0.6rem',
          }
        }}
      />
    </Box>
  );
}

export default SearchInput;
