import { Story, Meta } from '@storybook/react';
import { SearchInput, SearchInputProps } from './search-input';

export default {
  component: SearchInput,
  title: 'SearchInput',
} as Meta;

const Template: Story<SearchInputProps> = (args) => <SearchInput {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  onSearch: value => console.log(`searched: ${value}`)
};
