import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { transformBusinesses } from '@react-v2/utilities'
import { transformedFavorites, yelpBusinessSearchData } from '@react-v2/mock-data'

import BusinessesList from './businesses-list'

const businesses = transformBusinesses(yelpBusinessSearchData.businesses)

describe('BusinessesList', () => {
  it('should render successfully', () => {
    
    const { baseElement } = render(
      <BusinessesList
        businesses={businesses}
        favorites={transformedFavorites}
        addFavorite={business => null}
        removeFavorite={firebaseId => null}
      />
    );
    expect(baseElement).toBeTruthy();
  });

  it('should allow a user to favorite a business', () => {
    const addFavorite = jest.fn(business => null)
    render(
      <BusinessesList
        businesses={businesses}
        favorites={[]}
        addFavorite={addFavorite}
        removeFavorite={firebaseId => null}
      />
    )
    const favoriteButton = screen.getAllByRole('button')[0]
    userEvent.click(favoriteButton)
    expect(addFavorite).toBeCalledTimes(1)
  })

  it('should allow a user to unfavorite a business', () => {
    const removeFavorite = jest.fn(business => null)
    render(
      <BusinessesList
        businesses={businesses}
        favorites={transformedFavorites}
        addFavorite={business => null}
        removeFavorite={removeFavorite}
      />
    )
    const favoriteButton = screen.getAllByRole('button')[0]
    userEvent.click(favoriteButton)
    expect(removeFavorite).toBeCalledTimes(1)
  })
});
