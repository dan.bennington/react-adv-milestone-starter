import { Business, Favorite } from '@react-v2/types'
import {
  Card,
  CardContent,
  CardMedia,
  Chip,
  Grid,
  Stack,
  Typography
} from '@mui/material'
import { Stars } from '@mui/icons-material'
import FavoriteButton from '../favorite-button/favorite-button'

export interface BusinessesListProps {
  businesses: Business[];
  favorites: Favorite[];
  addFavorite: (business: Business) => void;
  removeFavorite: (firebaseId: string) => void;
}

export function BusinessesList({
  businesses,
  favorites,
  addFavorite,
  removeFavorite,
}: BusinessesListProps) {
  const openBusinesses = businesses.filter(({ isClosed }) => !isClosed)
  return (
    <Grid
      container
      spacing={3}
      sx={{
        justifyContent: 'center',
      }}
    >
      {openBusinesses.map(business => {
        const { id, name, image, address, phone, rating, categories } = business
        const favorite = favorites.find(fav => fav.id === id)
        return (
          <Grid item key={id}>
            <Card
              sx={{
                display: 'flex',
                flexDirection: 'column',
                width: 420,
                height: '100%',
              }}
            >
              <CardContent sx={{ padding: 0 }}>
                <CardMedia
                  component='img'
                  image={image}
                  height={240}
                  alt={name}
                />
              </CardContent>
              <CardContent
                sx={{
                  height: '100%',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'space-between'
                }}
              >
                <Stack sx={{ marginBottom: '2.4rem' }}>
                  <Stack
                    direction='row'
                    alignItems='center'
                    sx={{ width: '100%' }}
                  >
                    <Typography
                      sx={{
                        fontSize: '1.2rem',
                        fontWeight: '600',
                        width: '100%',
                        overflow: 'hidden',
                        display: '-webkit-box',
                        WebkitLineClamp: '1',
                        WebkitBoxOrient: 'vertical',
                      }}
                    >{name}</Typography>
                    <FavoriteButton
                      addFavorite={() => addFavorite(business)}
                      removeFavorite={() => removeFavorite(favorite?.firebaseId || '')}
                      isFavorite={!!favorite}
                    />
                  </Stack>
                  <Stack sx={{ marginBottom: '0.9rem' }}>                    
                    <Stack>
                      {address.map(addressLine => (
                        <Typography
                          key={addressLine}
                        >{addressLine}</Typography>
                      ))}
                    </Stack>
                    <Typography>{phone}</Typography>
                  </Stack>
                  <Stack
                    direction='row'
                    alignItems='center'
                  >
                    <Stars sx={{ marginRight: '0.3rem' }} />
                    <Typography>{rating}</Typography>
                  </Stack>
                </Stack>
                <Grid
                  container
                  spacing={1}
                >
                  {categories.map(category => (
                    <Grid item key={category}>
                      <Chip
                        label={category}
                        variant='outlined'
                      />
                    </Grid>
                  ))}
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        )
      })}
    </Grid>
  );
}

export default BusinessesList;
