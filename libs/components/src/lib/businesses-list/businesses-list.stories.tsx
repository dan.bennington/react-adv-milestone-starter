import { Story, Meta } from '@storybook/react';
import { BusinessesList, BusinessesListProps } from './businesses-list';
import { yelpBusinessSearchData } from '@react-v2/mock-data';
import { transformBusinesses } from '@react-v2/utilities'

const businesses = transformBusinesses(yelpBusinessSearchData.businesses)

export default {
  component: BusinessesList,
  title: 'BusinessesList',
} as Meta;

const Template: Story<BusinessesListProps> = (args) => (
  <BusinessesList {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  businesses,
  favorites: [],
  addFavorite: b => console.log('favorite added'),
  removeFavorite: id => console.log('favorite removed'),
};

export const OverflowCategories = Template.bind({});
OverflowCategories.args = {
  businesses: businesses.map(business => ({
    ...business,
    categories: [ ...business.categories, ...(business.categories.map(c => c + 'duplicate')) ]
  })),
  favorites: [],
  addFavorite: b => console.log('favorite added'),
  removeFavorite: id => console.log('favorite removed'),
};

export const Favorited = Template.bind({});
Favorited.args = {
  businesses,
  favorites: [ { ...businesses[0], firebaseId: 'id' } ],
  addFavorite: b => console.log('favorite added'),
  removeFavorite: id => console.log('favorite removed'),
};
