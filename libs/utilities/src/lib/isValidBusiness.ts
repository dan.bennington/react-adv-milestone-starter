import { Business } from '@react-v2/types'

const businessProperties: (keyof Business)[] = [
  'id',
  'name',
  'isClosed',
  'image',
  'address',
  'phone',
  'rating',
  'categories',
]

export const isValidBusiness = (business: Business) => (
  businessProperties.every(prop => (
    Object.prototype.hasOwnProperty.call(business, prop)
     && business[prop] !== null
  ))
)
