import { Favorite } from '@react-v2/types'

export const transformedFavorites: Favorite[] = [
  {
    address: [
        "809 W Randolph St",
        "Chicago, IL 60607"
    ],
    categories: [
        "American (New)",
        "Bakeries",
        "Coffee & Tea"
    ],
    id: "qjnpkS8yZO8xcyEIy5OU9A",
    image: "https://s3-media1.fl.yelpcdn.com/bphoto/ya6gjD4BPlxe7AKMj_5WsA/o.jpg",
    isClosed: false,
    name: "Girl & The Goat",
    phone: "+13124926262",
    rating: 4.5,
    firebaseId: "-Mxv7M-Ysw_H6uIlNawq",
  }
]
