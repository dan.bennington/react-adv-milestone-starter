import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import * as dataAccess from '@react-v2/data-access'
import { transformedFavorites } from '@react-v2/mock-data'

import App from './app'

afterEach(() => {
  jest.resetAllMocks()
})

jest.mock('firebase/app', () => ({
  initializeApp: () => ({})
}))

jest.mock('firebase/firestore', () => ({
  getFirestore: () => ({})
}))

describe('App', () => {
  it('should render successfully', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res(transformedFavorites)))

    const { baseElement } = render(<App />)
    const title = await screen.findByText('Yelp Lister')
    expect(baseElement).toBeTruthy()
    expect(title).toBeTruthy()
  });

  it('should render a list of businesses', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res(transformedFavorites)))

    render(<App />)
    const girlAndGoat = await screen.findByText('Girl & The Goat')
    const purplePig = await screen.findByText('The Purple Pig')
    expect(girlAndGoat).toBeTruthy()
    expect(purplePig).toBeTruthy()
  })

  it('should allow the user to filter businesses based on text input', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res(transformedFavorites)))

    render(<App />)
    const girlAndGoat = await screen.findByText('Girl & The Goat')
    expect(girlAndGoat).toBeTruthy()
    const searchInput = screen.getByRole('searchbox')
    userEvent.type(searchInput, 'pancakes{Enter}')
    expect(screen.queryByText('Girl & The Goat')).toBeFalsy()
    expect(screen.getByText('Wildberry Pancakes and Cafe')).toBeTruthy()
  })
});

describe('App favoriting integration', () => {
  it('should allow the user to favorite a business', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res([])))
      .mockReturnValueOnce(new Promise(res => res(transformedFavorites)))
  
    const addFavoriteSpy = jest.spyOn(dataAccess, 'addFavorite')
      .mockReturnValue(new Promise(res => {
        res({ message: 'success' })
      }))
  
    render(<App />)
    const favButtons = await screen.findAllByRole('button')
    const girlAndGoatFavButton = favButtons[0]
    userEvent.click(girlAndGoatFavButton)
    const filledGirlAndGoatFavoriteIcon = await screen.findByTestId('favorite-btn-filled')
    expect(filledGirlAndGoatFavoriteIcon).toBeTruthy()
    expect(addFavoriteSpy).toBeCalledTimes(1)
  })
  
  it('should not add business to favorites on addFavorite error', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res([])))
  
    jest.spyOn(dataAccess, 'addFavorite')
      .mockReturnValue(new Promise(res => {
        res(new Error())
      }))
  
    render(<App />)
    const favButtons = await screen.findAllByRole('button')
    const girlAndGoatFavButton = favButtons[0]
    userEvent.click(girlAndGoatFavButton)
    const emptyFavoriteIcons = await screen.findAllByTestId('favorite-btn-empty')
    const emptyGirlAndGoatFavoriteIcon = emptyFavoriteIcons[0]
    expect(emptyGirlAndGoatFavoriteIcon).toBeTruthy()
  })
  
  it('should handle error updating favorites list after favorite is added', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res([])))
      .mockReturnValueOnce(new Promise(res => {
        res(new Error())
      }))
  
    jest.spyOn(dataAccess, 'addFavorite')
      .mockReturnValue(new Promise(res => {
        res({ message: 'success' })
      }))
  
    render(<App />)
    const favButtons = await screen.findAllByRole('button')
    const girlAndGoatFavButton = favButtons[0]
    userEvent.click(girlAndGoatFavButton)
    const filledGirlAndGoatFavoriteIcon = await screen.findByTestId('favorite-btn-filled')
    expect(filledGirlAndGoatFavoriteIcon).toBeTruthy()
  })
  
  it('should allow the user to unfavorite a business', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValue(new Promise(res => {
        res(transformedFavorites)
      }))
  
    const removeFavoriteSpy = jest.spyOn(dataAccess, 'removeFavorite')
      .mockReturnValue(new Promise(res => {
        res({ message: 'success' })
      }))
  
    render(<App />)
    const favButtons = await screen.findAllByRole('button')
    const girlAndGoatFavButton = favButtons[0]
    userEvent.click(girlAndGoatFavButton)
    const emptyFavoriteIcons = await screen.findAllByTestId('favorite-btn-empty')
    const emptyGirlAndGoatFavoriteIcon = emptyFavoriteIcons[0]
    expect(emptyGirlAndGoatFavoriteIcon).toBeTruthy()
    expect(removeFavoriteSpy).toBeCalledTimes(1)
  })
  
  it('should not remove business from favorites on removeFavorite error', async () => {
    jest.spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise(res => res(transformedFavorites)))
  
    jest.spyOn(dataAccess, 'removeFavorite')
      .mockReturnValue(new Promise(res => {
        res(new Error())
      }))
  
    render(<App />)
    const favButtons = await screen.findAllByRole('button')
    const girlAndGoatFavButton = favButtons[0]
    userEvent.click(girlAndGoatFavButton)
    const filledGirlAndGoatFavoriteIcon = await screen.findByTestId('favorite-btn-filled')
    expect(filledGirlAndGoatFavoriteIcon).toBeTruthy()
  })
})
